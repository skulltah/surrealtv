# Surreal TV

Manages programmes and schedule in a clear and intuitive way.  
Automatically retrieves media metadata given a title.  

# Quickstart

To import my sample database, execute the following command: 'python manage.py loaddata db'.  
The admin credentials of said sample database are as following:  
- Username: Admin  
- Password: nextgear  
- Email: everydayanchovies@gmail.com  

# Admin management

This project includes an intuitive admin interface.  
The control panel can be accessed under /admin.  

### Scheduling a programme / movie

To schedule a movie, click on "Manage movies and their scheduling" and then click on a movie.  
You will see, alongside the movie's basic metadata, a list of calendar points. Add new ones to schedule the selected movie.  


### Changing the date / time or channel of a scheduled programme

To edit a programme's scheduling, click on Edit schedule and edit the appropriate calendar point.  

# About this project

The backend was built using Django. I've used SASS for the frontend development.  
Both backend and frontend have been developed by me.  
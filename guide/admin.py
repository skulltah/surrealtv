from django.contrib import admin
from django.contrib.contenttypes.admin import GenericTabularInline
from .models import AgendaPoint, Channel, Movie, Show, RadioShow

AGENDA_HELP_TEXT = ' '.join([
"""
<p><strong>Use this page to reschedule a programme. 
<br/>To add a new programme to a channel\'s agenda, 
use the appropriate medium page (the movie\'s page, for example.)</strong></p>
"""
])


"""
Admin page of a scheduled programme.
"""
class AgendaPointAdmin(admin.ModelAdmin):
    list_display = ('name', 'channel', 'content_type', 'start_date')
    search_fields = ('name', 'description')
    date_hierarchy = 'start_date'
    ordering = ('start_date',)
    list_filter = ('channel',)
    fieldsets = [
        ('Edit a scheduled programme', {
            'fields':('start_date', 'end_date', 'channel', 'name', 'description'),
            'description': '<div class="help">%s</div>' % AGENDA_HELP_TEXT,
        }),
    ]
    
    def has_add_permission(self, request):
        return False


"""
Inline mini scheduled programme inside a movie's admin page, or a show.
"""
class AgendaPointInline(GenericTabularInline):
    model = AgendaPoint


"""
Admin page for a channel.
"""
class ChannelAdmin(admin.ModelAdmin):
    list_display = ('name', 'content_type')
    search_fields = ('name', 'description')
    list_filter = ('content_type',)
    empty_value_display = '-empty-'
    fields = ('name', 'description', 'content_type', 'identifier', 'frequency')


"""
Admin page for a movie.
"""
class MovieAdmin(admin.ModelAdmin):
    inlines = [
        AgendaPointInline,
    ]


"""
Admin page for a show (series).
"""
class ShowAdmin(admin.ModelAdmin):
    inlines = [
        AgendaPointInline,
    ]


"""
Admin page for a radio show.
"""
class RadioShowAdmin(admin.ModelAdmin):
    inlines = [
        AgendaPointInline,
    ]


admin.site.register(Movie, MovieAdmin)
admin.site.register(Show, ShowAdmin)
admin.site.register(RadioShow, RadioShowAdmin)
admin.site.register(Channel, ChannelAdmin)
admin.site.register(AgendaPoint, AgendaPointAdmin)
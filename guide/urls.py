from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('channel/<int:channel_id>/', views.detail_channel, name='detail_channel'),
    path('movies/<int:movie_id>/', views.detail_movie, name='detail_movie'),
    path('shows/<int:show_id>/', views.detail_show, name='detail_show'),
    path('radio/<int:radio_show_id>/', views.detail_radio_show, name='detail_radio_show'),
    path('agenda/<int:agenda_point_id>/', views.detail_point, name='detail_point'),
    path('movies/', views.movies, name='movies'),
    path('shows/', views.shows, name='shows'),
    path('radio_shows/', views.radio_shows, name='radio_shows'),
]
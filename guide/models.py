from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericRelation
from django.core.exceptions import ValidationError


"""
Contains information about a single broadcasting channel.
"""
class Channel(models.Model):
    """
    Stores a single channel.
    """
    content_type = models.CharField(max_length=256, choices=[('video', 'Movies and shows'), ('radio', 'Radio')])
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=500)
    frequency = models.CharField(max_length=100, blank=True, help_text="Radio frequency (ignore if not applicable). Example: 952.2.")
    identifier = models.CharField(max_length=100, blank=True, help_text="Channel on TV (ignore if not applicable). Example: 24.")

    class Meta:
        verbose_name = "a channel"
        verbose_name_plural = "Manage channels"

    def __str__(self):
        return self.name
    
    def clean(self):
        if self.content_type != "radio" and self.content_type != "video":
            raise ValidationError("Content type must be either 'video' or 'radio'")


"""
Contains information about a single scheduled programme.
"""
class AgendaPoint(models.Model):
    start_date = models.DateTimeField('start date')
    end_date = models.DateTimeField('end date')
    name = models.CharField(max_length=200, help_text="Title of the medium (movie, show, etc.)", default="Title") 
    description = models.CharField(max_length=500, help_text="A brief description.", default="Description")
    channel = models.ForeignKey(Channel, on_delete=models.CASCADE)

    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, editable=False)
    object_id = models.PositiveIntegerField(editable=False)
    content_object = GenericForeignKey('content_type', 'object_id')

    class Meta:
        ordering = ['start_date']
        verbose_name = "a programme"
        verbose_name_plural = "Edit schedule"
    
    def __str__(self):
        return self.name
    
    def clean(self):
        if self.start_date > self.end_date:
            raise ValidationError("Dates are incorrect")


"""
Contains information about a single movie.
"""
class Movie(models.Model):
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=500)
    agenda_point = GenericRelation(AgendaPoint, related_query_name='agenda_point')
    
    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "a movie"
        verbose_name_plural = "Manage movies and their scheduling"


"""
Contains information about a single show (series).
"""
class Show(models.Model):
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=500)
    agenda_point = GenericRelation(AgendaPoint, related_query_name='agenda_point')
    
    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "a show"
        verbose_name_plural = "Manage shows and their scheduling"


"""
Contains information about a single radio show.
"""
class RadioShow(models.Model):
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=500)
    agenda_point = GenericRelation(AgendaPoint, related_query_name='agenda_point')
    
    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "a radio show"
        verbose_name_plural = "Manage radio shows and their scheduling"
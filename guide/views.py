from django.shortcuts import render
from django.http import HttpResponse, Http404
from django.shortcuts import render, get_object_or_404, get_list_or_404
from django.contrib.contenttypes.models import ContentType

import json

from .models import AgendaPoint, Channel, Movie, Show, RadioShow
from .rotten_tomatoes import get_rotten_movie


"""
Main page of programme guide. Shows a list of channels.
"""
def index(request):
    video_channels = Channel.objects.filter(content_type="video").order_by('-name')[:15]
    radio_channels = Channel.objects.filter(content_type="radio").order_by('-name')[:15]
    context = {'video_channels': video_channels, 'radio_channels': radio_channels}
    return render(request, 'guide/index.html', context)


"""
Shows a channel and its agenda.
"""
def detail_channel(request, channel_id):
    channel = get_object_or_404(Channel, pk=channel_id)

    try:
        agenda = AgendaPoint.objects.filter(channel=channel_id).order_by('start_date')[:30]
    except AgendaPoint.DoesNotExist:
        agenda = None

    context = {'agenda': agenda, 'channel': channel}
    return render(request, 'guide/agenda.html', context)


"""
Shows a movie in detail.
Retrieves information about the movie from an API and displays it neatly.
"""
def detail_movie(request, movie_id):
    movie = get_object_or_404(Movie, pk=movie_id)
    # get movie metadata from third party API
    rotten_tomatoes = get_rotten_movie(movie.name)
    
    content_type = ContentType.objects.get_for_model(Movie)

    try:
        agenda = AgendaPoint.objects.filter(content_type=content_type, object_id=movie.id).order_by('start_date')[:30]
    except AgendaPoint.DoesNotExist:
        agenda = None

    return render(request, 'guide/detail_movie.html', {'movie': movie, 'rotten_tomatoes': rotten_tomatoes, 'agenda_points': agenda})


"""
Shows a show (series) in detail.
Retrieves information about the series from an API and displays it neatly.
"""
def detail_show(request, show_id):
    show = get_object_or_404(Show, pk=show_id)
    # get show metadata from third party API
    rotten_tomatoes = get_rotten_movie(show.name)
    
    content_type = ContentType.objects.get_for_model(Show)
    try:
        agenda = AgendaPoint.objects.filter(content_type=content_type, object_id=show.id).order_by('start_date')[:30]
    except AgendaPoint.DoesNotExist:
        agenda = None

    return render(request, 'guide/detail_show.html', {'show': show, 'rotten_tomatoes': rotten_tomatoes, 'agenda_points': agenda})


"""
Displays a radio show in detail.
"""
def detail_radio_show(request, radio_show_id):
    radio_show = get_object_or_404(RadioShow, pk=radio_show_id)
    
    content_type = ContentType.objects.get_for_model(RadioShow)
    try:
        agenda = AgendaPoint.objects.filter(content_type=content_type, object_id=radio_show.id).order_by('start_date')[:30]
    except AgendaPoint.DoesNotExist:
        agenda = None

    return render(request, 'guide/detail_radio_show.html', {'radio_show': radio_show, 'agenda_points': agenda})


"""
Displays a scheduled programme in detail.
Retrieves extra information from a third party API, if possible.
"""
def detail_point(request, agenda_point_id):
    agenda_point = get_object_or_404(AgendaPoint, pk=agenda_point_id)

    try:
        movie = Movie.objects.get(agenda_point=agenda_point_id)
    except Movie.DoesNotExist:
        movie = None

    try:
        show = Show.objects.get(agenda_point=agenda_point_id)
    except Show.DoesNotExist:
        show = None

    try:
        radio_show = RadioShow.objects.get(agenda_point=agenda_point_id)
    except RadioShow.DoesNotExist:
        radio_show = None
        
    if movie != None: 
        # get movie metadata from third party API
        rotten_tomatoes = get_rotten_movie(movie.name)
        return render(request, 'guide/detail_movie.html', {'agenda_point': agenda_point, 'movie': movie, 'rotten_tomatoes': rotten_tomatoes})
    elif show != None:
        # get show metadata from third party API
        rotten_tomatoes = get_rotten_movie(show.name)
        return render(request, 'guide/detail_show.html', {'agenda_point': agenda_point, 'show': show, 'rotten_tomatoes': rotten_tomatoes})
    elif radio_show != None:
        return render(request, 'guide/detail_radio_show.html', {'agenda_point': agenda_point, 'radio_show': radio_show})
    else:
        return render(request, 'guide/detail_point.html', {'agenda_point': agenda_point})


"""
Displays a list of all available movies.
"""
def movies(request):
    try:
        movies = Movie.objects.all()
    except Movie.DoesNotExist:
        movies = None
    return render(request, 'guide/movies.html', {'movies': movies})


"""
Displays a list of all available shoes (series).
"""
def shows(request):
    try:
        shows = Show.objects.all()
    except Show.DoesNotExist:
        shows = None
    return render(request, 'guide/shows.html', {'shows': shows})


"""
Displays a list of all available radio shows.
"""
def radio_shows(request):
    try:
        radio_shows = RadioShow.objects.all()
    except RadioShow.DoesNotExist:
        radio_shows = None
    return render(request, 'guide/radio_shows.html', {'radio_shows': radio_shows})
import requests

"""
Retrieves information about a movie or a show given the title.
Rotten tomatoes API is not publicly available so I'll be using a placeholder alternative
"""
def get_rotten_movie(movie_title):
    response = requests.get('http://www.omdbapi.com/?t=' + movie_title + '&apikey=a3bcb255')
    return response.json()